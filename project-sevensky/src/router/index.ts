import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/productcatalog',
    name: 'Product Catalog',
    component: () => import('../views/Catalog.vue')
  },
  {
    path: '/faq',
    name: 'FAQ',
    component: () => import('../views/FAQ.vue')
  },
  {
    path: '/contact',
    name: 'Contact Us',
    component: () => import('../views/Contact.vue')
  },
  {
    path: '/spec',
    name: 'Speficication',
    component: () => import('../views/Specification.vue')
  },
  {
    path: '/spec1',
    name: 'Speficication1',
    component: () => import('../views/Spec1.vue')
  },
  {
    path: '/spec2',
    name: 'Speficication2',
    component: () => import('../views/Spec2.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
