import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const messages = {
  en: {
    faqOffer: 'How can we help?',
    serviceSupport: 'Service support',
    prioritySupport: 'Priority service support',
    phoneSupport: 'Phone and service support',
    helpCenter: 'Help center access',
    buyButton: 'Buy',
    purchaseText: 'Purchasing process',
    deliveryText: 'Delivery process',
    purchaseProcess: 'Purchases can be made at the nearest store in your area. Please provide enough money so that you can buy the desired product. The products sold are always EXIST.',
    deliveryProcess: 'Delivery can be made if you have paid for the product you are going to buy. If it has been paid, our party will confirm via your email and you will receive a payment receipt. Please provide your full address for delivery according to destination.',
    homeOneSkyText: 'First generation smartwatch. It’ll blow your mind.',
    homeTwoSkyText: 'Oh yeah, it’s that good. See for yourself.',
    homeThreeSkyText: 'And lastly, this one. Checkmate.',
    homeDetailBtn: 'View details',
    catalogTrailerVideo: 'See the trailer of SevenSky Smart Watch',
    catalogSpecBtn: 'Specification',
    contactUs: 'Contact Us',
    contactText: 'Fill up the form and our team will get back to you within 24 hours.',
    contactInfo: 'Contact Information',
    contactFirstName: 'First name',
    contactLastName: 'Last name',
    contactEmail: 'Email',
    contactMessage: 'Message',
    contactSendBtn: 'Send'
  },
  id: {
    faqOffer: 'Bagaimana kami dapat membantu?',
    serviceSupport: 'Dukungan layanan',
    prioritySupport: 'Prioritas dukungan layanan',
    phoneSupport: 'Dukungan telepon dan layanan',
    helpCenter: 'Akses pusat bantuan',
    buyButton: 'Beli',
    purchaseText: 'Proses pembelian',
    deliveryText: 'Proses pengiriman',
    purchaseProcess: 'Pembelian dapat dilakukan di toko terdekat di daerah anda. Mohon sediakan uang yang cukup sehingga anda dapat membeli produk yang diinginkan. Produk yang dijual selalu ADA.',
    deliveryProcess: 'Pengiriman dapat dilakukan di jika anda telah membayar produk yang akan anda beli. Jika telah terbayar, maka pihak kami akan melakukan konfirmasi melalui email anda dan anda akan menerima nota pembayaran. Mohon sediakan alamat lengkap anda agar pengiriman sesuai tujuan.',
    homeOneSkyText: 'Generasi pertama smartwatch. Ini akan membuat Anda terpana.',
    homeTwoSkyText: 'Oh ya, itu bagus. Lihatlah sendiri.',
    homeThreeSkyText: 'Dan terakhir, yang ini. Sekakmat.',
    homeDetailBtn: 'Lihat detail',
    catalogTrailerVideo: 'Lihat cuplikan SevenSky Smart Watch',
    catalogSpecBtn: 'Spesifikasi',
    contactUs: 'Kontak kami',
    contactText: 'Isi formulir dan tim kami akan menghubungi Anda dalam waktu 24 jam.',
    contactInfo: 'Informasi Kontak',
    contactFirstName: 'Nama depan',
    contactLastName: 'Nama belakang',
    contactEmail: 'Email',
    contactMessage: 'Pesan',
    contactSendBtn: 'Kirim'
  }
}

const i18n = new VueI18n({
  locale: 'en',
  fallbackLocale: 'id',
  messages
})

export default i18n
